package org.ncompass.comp.cmutls.staticfns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang3.exception.ExceptionUtils;
import java.util.UUID;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class GenericUtilities {

	private static final Logger logger = LoggerFactory.getLogger(GenericUtilities.class);

	public static boolean isNull(Object value) {

		if (value == null) {
			return true;
		}

		return false;
	}

	public static String objectToString(Object value) {

		if (value == null) {
			return null;
		}

		return value.toString();

	}

	public static long objectToLong(Object value) {

		if (value == null) {
			return 0;
		}

		return stringToLong(value.toString());

	}

	public static String byteToString(byte[] b) {

		if (b == null || b.length == 0) {
			return null;
		}

		try {
			return new String(b, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("byteToStringException>>", e);
		}
		return null;

	}

	public static void trimLastChars(StringBuilder sb, int numberOfChars) {
		if (sb == null)
			return;
		if (sb.length() <= numberOfChars) {
			return;
		}
		sb.setLength(sb.length() - numberOfChars);
	}

	public static boolean contains(String value, String check) {
		if (value == null) {
			return false;
		}
		if (check == null) {
			return false;
		}
		return value.contains(check);
	}

	public static String trimLastChars(String sb, int numberOfChars) {
		if (sb == null)
			return null;
		if (sb.length() <= numberOfChars) {
			return null;
		}
		return sb.substring(0, sb.length() - numberOfChars);
	}

	public static boolean isEquals(String arg1, String arg2) {
		if (arg1 == null)
			return false;
		if (arg2 == null)
			return false;
		return arg1.equals(arg2);
	}

	public static boolean isEmpty(Object value) {
		if (value == null || value.toString().equals("")) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(String value) {
		if (value == null || value.equals("")) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(Set<?> set) {
		if (set == null) {
			return true;
		}
		return set.isEmpty();
	}

	public static boolean isEmpty(List<?> list) {
		if (list == null) {
			return true;
		}
		return list.isEmpty();
	}

	public static boolean isEmpty(Map<?, ?> map) {
		if (map == null) {
			return true;
		}
		return map.isEmpty();
	}

	public static <T> boolean isEmpty(T[] value) {
		if (value == null || value.length == 0) {
			return true;
		}

		return false;
	}

	public static <T> int sizeOf(T[] value) {

		if (isEmpty(value))
			return 0;
		return value.length;

	}

	public static int sizeOf(String value) {
		if (isEmpty(value))
			return 0;
		return value.length();
	}

	public static int sizeOf(Set<?> set) {
		if (set == null) {
			return 0;
		}
		return set.size();
	}

	public static int sizeOf(List<?> list) {
		if (list == null) {
			return 0;
		}
		return list.size();
	}

	public static int sizeOf(Map<?, ?> map) {
		if (map == null) {
			return 0;
		}
		return map.size();
	}

	public static <K, V> V valueFromMap(Map<K, V> map, K key) {
		if (isEmpty(map)) {
			return null;
		}

		return map.get(key);
	}

	public static <K, V> String valueAsStringFromMap(Map<K, V> map, K key) {

		return objectToString(valueFromMap(map, key));

	}

	public static <E> E firstInList(List<E> list) {
		if (isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	public static String nullToNullString(String arg1) {
		return arg1 == null ? "" : arg1;
	}

	public static int IntegerToInt(Integer value) {
		if (value == null) {
			return 0;
		}
		return value.intValue();
	}

	public static BigDecimal stringToBigDecimal(String value, String defaultValue) {
		if (isEmpty(value)) {
			return stringToBigDecimal(defaultValue);
		}
		try {
			return new BigDecimal(value);
		} catch (Exception e) {
			logger.error("stringToBigDecimal>>{}", value, e);
		}
		return stringToBigDecimal(defaultValue);
	}

	public static BigDecimal stringToBigDecimal(String value) {
		if (isEmpty(value)) {
			return null;
		}
		try {
			return new BigDecimal(value);
		} catch (Exception e) {
			logger.error("stringToBigDecimal>>{}", value, e);
		}
		return null;
	}

	public static BigDecimal stringToBigDecimalThrows(String value) {
		return new BigDecimal(value);
	}

	public static int stringToInt(String value) {
		if (isEmpty(value)) {
			return 0;
		}
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			logger.error("stringToInt>>{}", value, e);
		}
		return 0;
	}

	public static int stringToIntThrows(String value) {
		return Integer.parseInt(value);
	}

	public static boolean stringToBoolean(String value) {
		if (isEmpty(value)) {
			return false;
		}
		try {
			return Boolean.parseBoolean(value);
		} catch (Exception e) {
			logger.error("stringToBoolean>>{}", value, e);
		}
		return false;
	}

	public static String stringToIntString(String value) {
		return String.valueOf(stringToInt(value));
	}

	public static float stringToFloat(String value) {
		if (isEmpty(value)) {
			return 0;
		}
		try {
			return Float.parseFloat(value);
		} catch (Exception e) {
			logger.error("stringToFloat>>{}", value, e);
		}
		return 0;
	}

	public static float stringToFloatThrows(String value) {
		return Float.parseFloat(value);
	}

	public static double stringToDouble(String value) {
		if (isEmpty(value)) {
			return 0;
		}
		try {
			return Double.parseDouble(value);
		} catch (Exception e) {
			logger.error("stringToDouble>>{}", value, e);
		}
		return 0;
	}

	public static double stringToDoubleThrows(String value) {
		return Double.parseDouble(value);
	}

	public static String stringToDoubleString(String value) {
		return String.valueOf(stringToDouble(value));
	}

	public static long stringToLong(String value) {
		if (isEmpty(value)) {
			return 0;
		}
		try {
			return Long.parseLong(value);
		} catch (Exception e) {
			logger.error("stringToLong>>{}", value, e);
		}
		return 0;
	}

	public static long stringToLongThrows(String value) {
		return Long.parseLong(value);
	}

	public static String stringToLongString(String value) {
		return String.valueOf(stringToLong(value));
	}

	public static String UUID() {
		return UUID.randomUUID().toString();
	}

	public static String getErrorMessage(Exception e) {
		if (e == null) {
			return "Internal error.";
		}

		String message = e.getMessage();
		if (GenericUtilities.isEmpty(message)) {
			return e.getClass().getSimpleName() + ": Internal error.";
		}
		return message;
	}

	public static String getRootCauseMessage(Exception e) {
		if (e == null) {
			return "Internal error.";
		}
		Throwable root = ExceptionUtils.getRootCause(e);
		root = root == null ? e : root;
		String rootCauseMessage = root.getMessage();
		if (GenericUtilities.isEmpty(rootCauseMessage)) {
			return root.getClass().getSimpleName() + ": Internal error.";
		}
		return rootCauseMessage;
	}

	public static <E> String convertArrayListtoCSV(ArrayList<E> list) {
		if (list == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();

		for (E e : list) {

			if (isEmpty(e)) {
				continue;
			}
			sb.append(e);
			sb.append(",");

		}

		trimLastChars(sb, 1);
		return sb.toString();
	}

	public static ArrayList<String> convertCSVtoArrayList(String csv) {
		if (isEmpty(csv)) {
			return null;
		}
		String[] csvArray = csv.split(",");
		if (csvArray == null || csvArray.length == 0) {
			return null;
		}
		ArrayList<String> list = new ArrayList<String>();
		for (String value : csvArray) {
			list.add(value);
		}
		return list;
	}

}
