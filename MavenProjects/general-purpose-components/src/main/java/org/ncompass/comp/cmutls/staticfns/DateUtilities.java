package org.ncompass.comp.cmutls.staticfns;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class DateUtilities {

	public static final String DATE_TIME_FORMAT_COMMON_DISPLAY = "dd-MMM-yyyy hh:mm:ss a";

	private static final Logger logger = LoggerFactory.getLogger(DateUtilities.class);

	public static java.sql.Time localTimeToSqlTime(LocalTime lt) {

		return (lt == null) ? null : java.sql.Time.valueOf(lt);

	}

	public static LocalTime sqlTimeToLocalTime(java.sql.Time t) {

		return (t == null) ? null : t.toLocalTime();

	}

	public static LocalDateTime sqlTimestampToLocalDateTime(java.sql.Timestamp ts) {

		return (ts == null) ? null : ts.toLocalDateTime();

	}

	public static java.sql.Timestamp localDateTimeToSqlTimestamp(LocalDateTime ldt) {

		return (ldt == null) ? null : java.sql.Timestamp.valueOf(ldt);

	}

	public static LocalDate sqlDateToLocalDate(java.sql.Date d) {

		return (d == null) ? null : d.toLocalDate();

	}

	public static java.sql.Date localDateToSqlDate(LocalDate ld) {

		return (ld == null) ? null : java.sql.Date.valueOf(ld);

	}

	public static LocalTime epochMilliToLocalTime(long value) {

		if (value < 1) {
			return null;
		}

		try {

			return LocalTime.ofInstant(epochMilliToInstant(value), ZoneId.systemDefault());

		} catch (Exception e) {
			logger.error("epochMilliToLocalTime>>{}", value, e);
		}

		return null;

	}

	public static long localTimeToEpochMilli(LocalTime lt) {

		if (lt == null) {
			return 0;
		}

		try {

			long epochMilli = lt.atDate(LocalDate.EPOCH).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

			return epochMilli;

		} catch (Exception e) {
			logger.error("localTimeToEpochMilli>>{}", lt, e);
		}

		return 0;

	}

	public static LocalDate epochMilliToLocalDate(long value) {

		if (value < 1) {
			return null;
		}

		try {

			return LocalDate.ofInstant(epochMilliToInstant(value), ZoneId.systemDefault());

		} catch (Exception e) {
			logger.error("epochMilliToLocalDate>>{}", value, e);
		}

		return null;

	}

	public static long localDateToEpochMilli(LocalDate ld) {

		if (ld == null) {
			return 0;
		}

		try {

			ZonedDateTime zdt = ld.atStartOfDay(ZoneId.systemDefault());

			return zdt.toInstant().toEpochMilli();

		} catch (Exception e) {
			logger.error("localDateToEpochMilli>>{}", ld, e);
		}

		return 0;

	}

	public static LocalDateTime epochMilliToLocalDateTime(long value) {

		if (value < 1) {
			return null;
		}

		try {

			return LocalDateTime.ofInstant(epochMilliToInstant(value), ZoneId.systemDefault());

		} catch (Exception e) {
			logger.error("epochMilliToLocalDateTime>>{}", value, e);
		}

		return null;

	}

	public static long localDateTimeToEpochMilli(LocalDateTime ldt) {

		if (ldt == null) {
			return 0;
		}

		try {

			ZonedDateTime zdt = ZonedDateTime.of(ldt, ZoneId.systemDefault());
			return zdt.toInstant().toEpochMilli();

		} catch (Exception e) {
			logger.error("localDateTimeToEpochMilli>>{}", ldt, e);
		}

		return 0;

	}

	public static String localTimeToString(LocalTime lt, String format) {
		if (lt == null) {
			return "";
		}
		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return lt.format(formatter);

		} catch (Exception e) {
			logger.error("localTimeToString>>{}>>{}", lt, format, e);
		}
		return "";
	}

	public static String localDateTimeToString(LocalDateTime ldt, String format) {
		if (ldt == null) {
			return "";
		}
		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return ldt.format(formatter);

		} catch (Exception e) {
			logger.error("localDateTimeToString>>{}>>{}", ldt, format, e);
		}
		return "";
	}

	public static String localDateToString(LocalDate ld, String format) {
		if (ld == null) {
			return "";
		}
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return ld.format(formatter);

		} catch (Exception e) {
			logger.error("localDateToString>>{}>>{}", ld, format, e);
		}
		return "";
	}

	public static String zonedDateTimeToString(ZonedDateTime zdt, String format) {
		if (zdt == null) {
			return "";
		}
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return zdt.format(formatter);

		} catch (Exception e) {
			logger.error("zonedDateTimeToString>>{}>>{}", zdt, format, e);
		}
		return "";
	}

	public static ZonedDateTime stringToZonedDateTime(String value, String format, ZoneId zoneid) {

		if (GenericUtilities.isEmpty(value)) {
			return null;
		}

		if (zoneid == null) {
			return null;
		}

		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			ZonedDateTime zdt = ZonedDateTime.parse(value, formatter);
			return zdt.withZoneSameInstant(zoneid);

		} catch (Exception e) {
			logger.error("stringToZonedDateTime>>{}>>{}>>{}", value, format, zoneid, e);
		}
		return null;

	}

	public static ZonedDateTime stringToZonedDateTime(String value, String format) {

		if (GenericUtilities.isEmpty(value)) {
			return null;
		}

		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return ZonedDateTime.parse(value, formatter);

		} catch (Exception e) {
			logger.error("stringToZonedDateTime>>{}>>{}", value, format, e);
		}
		return null;

	}

	public static LocalDateTime stringToLocalDateTime(String value, String format) {

		if (GenericUtilities.isEmpty(value)) {
			return null;
		}

		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return LocalDateTime.parse(value, formatter);

		} catch (Exception e) {
			logger.error("stringToLocalDateTime>>{}>>{}", value, format, e);
		}
		return null;

	}

	public static LocalDate stringToLocalDate(String value, String format) {

		if (GenericUtilities.isEmpty(value)) {
			return null;
		}

		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return LocalDate.parse(value, formatter);

		} catch (Exception e) {
			logger.error("stringToLocalDate>>{}>>{}", value, format, e);
		}
		return null;

	}

	public static LocalTime stringToLocalTime(String value, String format) {

		if (GenericUtilities.isEmpty(value)) {
			return null;
		}

		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return LocalTime.parse(value, formatter);

		} catch (Exception e) {
			logger.error("stringToLocalTime>>{}>>{}", value, format, e);
		}
		return null;

	}

	public static Instant epochMilliToInstant(long value) {

		if (value < 1) {
			return null;
		}

		try {

			return Instant.ofEpochMilli(value);
		} catch (Exception e) {

			logger.error("milliToInstant>>{}", value, e);

		}

		return null;

	}

	public static ZoneId getZoneId(String zoneId, boolean getDefaultIfNull) {

		ZoneId zn = null;

		if (!GenericUtilities.isEmpty(zoneId)) {

			try {
				zn = ZoneId.of(zoneId);

			} catch (Exception e) {
				logger.error("getTimeZone>>{}", zoneId, e);
			}

		}

		if (zn == null && getDefaultIfNull) {
			zn = ZoneId.systemDefault();
		}

		return zn;

	}

	public static String systemDate(String format) {

		return localDateToString(LocalDate.now(), format);

	}

	public static String systemDateTime(String format) {

		return localDateTimeToString(LocalDateTime.now(), format);

	}

	public static String systemDateTimeZone(String format) {
		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			ZonedDateTime now = ZonedDateTime.now();
			String formattedString = now.format(formatter);

			return formattedString + " " + now.getZone() + " " + now.getOffset();
		} catch (Exception e) {
			logger.error("systemDateTimeZone>>", e);
		}
		return null;
	}

	public static long getMillisToThisHourAfterMidnight(int hour, String zone_id) {

		ZoneId zoneId = getZoneId(zone_id, true);
		LocalDateTime now = LocalDateTime.now(zoneId);

		LocalTime midnight = LocalTime.MIDNIGHT;
		midnight = midnight.minusSeconds(1);
		LocalDateTime todayMidnight = LocalDateTime.of(now.toLocalDate(), midnight);

		LocalDateTime midnightplusHours = todayMidnight.plusHours(hour);

		long millis = Duration.between(now, midnightplusHours).toMillis();

		return millis;

	}

}
