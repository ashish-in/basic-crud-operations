package org.ncompass.comp.cmutls.error;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class CommonRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 2036731829550323395L;
	private String errorMessage;

	public CommonRuntimeException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return getErrorMessage();
	}

}
