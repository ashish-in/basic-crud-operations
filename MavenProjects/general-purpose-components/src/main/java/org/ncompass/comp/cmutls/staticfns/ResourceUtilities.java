package org.ncompass.comp.cmutls.staticfns;

import java.io.Closeable;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
  @author anoop
  @company NCompass
  @date Mar 15, 2022  
 **/
public class ResourceUtilities {
	

	private static final Logger logger = LoggerFactory.getLogger(ResourceUtilities.class);

	public static void closeCloseable(Closeable cl) {
		try {
			if (cl != null)
				cl.close();
		} catch (Exception e) {
			logger.error("closeCloseable>>", e);
		}
	}

	public static void closeHttpURLConnection(HttpURLConnection huc) {
		try {
			if (huc != null)
				huc.disconnect();
		} catch (Exception e) {
			logger.error("closeHttpURLConnection>>", e);
		}
	}

	public static byte[] inputStreamToByteArray(InputStream is) {

		try {
			return IOUtils.toByteArray(is);
		} catch (Exception e) {
			logger.error("inputStreamToByteArray>>", e);
		} finally {
			closeCloseable(is);
		}
		return null;

	}

	public static byte[] getResourceBytes(String fileName) {

		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		return inputStreamToByteArray(classloader.getResourceAsStream(fileName));

	}

	public static HashMap<String, String> getResourceProperties(String fileName) {
		ResourceBundle resource = ResourceBundle.getBundle(fileName);
		HashMap<String, String> appProperties = new HashMap<String, String>();

		Enumeration<String> keys = resource.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			appProperties.put(key, resource.getString(key));
		}

		return appProperties;
	}



}
