package org.ncompass.comp.sprgjdbcwrpr.utilities;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import org.ncompass.comp.cmutls.staticfns.GenericUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class JdbcUtilities {

	private static final Logger logger = LoggerFactory.getLogger(JdbcUtilities.class);

	public static String getSQLErrorMessage(SQLException se) {
		if (se == null) {
			return GenericUtilities.getErrorMessage(se);
		}
		String errorMessage = GenericUtilities.getErrorMessage(se);
		errorMessage = (errorMessage == null) ? "" : errorMessage;
		String sQLState = se.getSQLState();
		sQLState = (sQLState == null) ? "" : sQLState;
		int errorCode = se.getErrorCode();
		return errorMessage + ">>" + sQLState + ">>" + errorCode;

	}

	public static String getTransactionErrorMessage(TransactionException te) {
		if (te == null) {
			return "Internal error.";
		}

		if (te.getCause() instanceof SQLException) {
			SQLException sqle = (SQLException) te.getCause();

			return getSQLErrorMessage(sqle);
		}

		Throwable root = te.getMostSpecificCause();
		root = root == null ? te : root;

		String rootCauseMessage = root.getMessage();
		if (GenericUtilities.isEmpty(rootCauseMessage)) {
			return root.getClass().getSimpleName() + ": Internal error.";
		}
		return rootCauseMessage;

	}

	public static String getTransactionErrorMessageNice(TransactionException te) {
		if (te == null) {
			return "Internal error.";
		}

		Throwable root = te.getMostSpecificCause();
		root = root == null ? te : root;

		String rootCauseMessage = root.getMessage();
		if (GenericUtilities.isEmpty(rootCauseMessage)) {
			return root.getClass().getSimpleName() + ": Internal error.";
		}
		return rootCauseMessage;

	}

	public static String getDataAccessErrorMessage(DataAccessException dae) {
		if (dae == null) {
			return "Internal error.";
		}

		if (dae.getCause() instanceof SQLException) {
			SQLException sqle = (SQLException) dae.getCause();

			return getSQLErrorMessage(sqle);
		}

		Throwable root = dae.getMostSpecificCause();
		root = root == null ? dae : root;

		String rootCauseMessage = root.getMessage();
		if (GenericUtilities.isEmpty(rootCauseMessage)) {
			return root.getClass().getSimpleName() + ": Internal error.";
		}
		return rootCauseMessage;

	}

	public static String getDataAccessErrorMessageNice(DataAccessException dae) {
		if (dae == null) {
			return "Internal error.";
		}

		Throwable root = dae.getMostSpecificCause();
		root = root == null ? dae : root;

		String rootCauseMessage = root.getMessage();
		if (GenericUtilities.isEmpty(rootCauseMessage)) {
			return root.getClass().getSimpleName() + ": Internal error.";
		}
		return rootCauseMessage;

	}

	public static String getSchemaName(String connectString) {
		if (GenericUtilities.isEmpty(connectString)) {
			return null;
		}
		try {
			String sanitizedString = null;
			int schemeEndOffset = connectString.indexOf("://");
			if (-1 == schemeEndOffset) {
				// couldn't find one? try our best here.
				sanitizedString = "http://" + connectString;
				logger.warn("Could not find database access scheme in connect string>>{}", connectString);
			} else {
				sanitizedString = "http" + connectString.substring(schemeEndOffset);
			}
			URL connectUrl = new URL(sanitizedString);
			String databaseName = connectUrl.getPath();
			if (null == databaseName) {
				return null;
			}
			// This is taken from a 'path' part of a URL, which may have leading
			// '/'
			// characters; trim them off.
			while (databaseName.startsWith("/")) {
				databaseName = databaseName.substring(1);
			}
			return databaseName;
		} catch (MalformedURLException mue) {
			logger.error("MalformedURLException>>{}", connectString, mue);
			return null;
		}
	}

	/**
	 * @return the hostname from the connect string, or null if we can't.
	 */
	public static String getHostName(String connectString) {
		try {
			String sanitizedString = null;
			int schemeEndOffset = connectString.indexOf("://");
			if (-1 == schemeEndOffset) {
				// Couldn't find one? ok, then there's no problem, it should
				// work as a
				// URL.
				sanitizedString = connectString;
			} else {
				sanitizedString = "http" + connectString.substring(schemeEndOffset);
			}
			URL connectUrl = new URL(sanitizedString);
			return connectUrl.getHost();
		} catch (MalformedURLException mue) {
			logger.error("MalformedURLException>>{}", connectString, mue);
			return null;
		}
	}

	/**
	 * @return the port from the connect string, or -1 if we can't.
	 */
	public static int getPort(String connectString) {
		try {
			String sanitizedString = null;
			int schemeEndOffset = connectString.indexOf("://");
			if (-1 == schemeEndOffset) {
				// Couldn't find one? ok, then there's no problem, it should
				// work as a
				// URL.
				sanitizedString = connectString;
			} else {
				sanitizedString = "http" + connectString.substring(schemeEndOffset);
			}
			URL connectUrl = new URL(sanitizedString);
			return connectUrl.getPort();
		} catch (MalformedURLException mue) {
			logger.error("MalformedURLException>>{}", connectString, mue);
			return -1;
		}
	}

	public static String getQuery(String connectString) {
		try {
			String sanitizedString = null;
			int schemeEndOffset = connectString.indexOf("://");
			if (-1 == schemeEndOffset) {
				// Couldn't find one? ok, then there's no problem, it should
				// work as a
				// URL.
				sanitizedString = connectString;
			} else {
				sanitizedString = "http" + connectString.substring(schemeEndOffset);
			}
			URL connectUrl = new URL(sanitizedString);
			return connectUrl.getQuery();
		} catch (MalformedURLException mue) {
			logger.error("MalformedURLException>>{}", connectString, mue);
			return null;
		}
	}

}
