package org.ncompass.comp.cmutls.helper;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class PrintLog {

	private StringBuilder sb;

	private PrintLog() {
		super();
		sb = new StringBuilder();

	}

	public static PrintLog build() {

		PrintLog pl = new PrintLog();
		return pl;

	}

	public static PrintLog buildN_Uln(String label) {

		PrintLog pl = new PrintLog();
		pl.appendN_Uln(label);
		return pl;

	}

	public static PrintLog buildN(String label, Object value) {

		PrintLog pl = new PrintLog();
		pl.appendN(label, value);
		return pl;

	}

	public PrintLog appendN(String label) {

		sb.append("\n").append(label);

		return this;

	}

	public PrintLog appendN_Uln(String label) {

		sb.append("\n").append(label);
		sb.append("\n-------------------------");

		return this;

	}

	public PrintLog appendN_Uln(String label, Object value) {

		sb.append("\n").append(label);
		sb.append("\n-------------------------");
		sb.append(value);

		return this;

	}

	public PrintLog appendN(String label, Object value) {
		sb.append("\n").append(label).append(">>").append(value);
		return this;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return sb.toString();
	}

}
