package org.ncompass.comp.sprgjdbcwrpr.pool;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import java.lang.management.ManagementFactory;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class HikariCPPool {

	private static final Logger logger = LoggerFactory.getLogger(HikariCPPool.class);
	private HikariDataSource dataSource;
	private Map<String, String> dataSourceProperties;
	private String poolName;

	public HikariCPPool(String poolName, Map<String, String> dataSourceProperties) {
		this.poolName = poolName;
		this.dataSourceProperties = dataSourceProperties;
		logger.info("*{}*created-instance", poolName);
	}

	public synchronized void createDataSource() throws SQLException {
		if (dataSource == null) {
			try {
				HikariConfig hc = getJdbcPoolProperties();
				hc.setPoolName(getPoolName());
				hc.setRegisterMbeans(true);
				dataSource = new HikariDataSource(hc);
				testConnection();

				logger.info("*{}*createDataSourceGood>>", getPoolName());

			} catch (Exception e) {
				logger.error("*{}*createDataSource>>", getPoolName(), e);
				dataSource = null;
				throw e;
			}
		} else {
			logger.warn("*{}*DataSourceAlreadyExists>>", getPoolName());
		}
		logger.info("createDataSource>>StateOfDataSource>>\n" + getStateOfDataSource());
	}

	public void closeDataSource() {
		if (dataSource == null) {
			logger.warn("*{}*DataSourceNotExistsForClose>>", getPoolName());
			return;
		}
		logger.info("closeDataSource>>StateOfDataSource>>\n" + getStateOfDataSource());
		dataSource.close();
		logger.info("*{}*closeDataSourceGood>>", getPoolName());
		dataSource = null;
	}

	private Connection getConnection() throws SQLException {
		if (dataSource == null) {
			throw new SQLException("Data source is null");
		}
		return dataSource.getConnection();
	}

	public boolean testConnection() throws SQLException {

		Connection conn = null;
		try {
			conn = getConnection();
			return true;

		} finally {
			closeConnection(conn);
		}

	}

	private void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			logger.error("closeConnection>>", e);
		}
		conn = null;
	}

	public Map<String, Object> getStateOfDataSource() {
		Map<String, Object> stateOfDataSource = new LinkedHashMap<String, Object>();
		stateOfDataSource.put("pool_name", getPoolName());
		stateOfDataSource.put("is_data_source_exists", false);
		try {
			if (dataSource != null) {
				stateOfDataSource.put("is_data_source_exists", true);
				stateOfDataSource.put("jdbc_url", dataSource.getJdbcUrl());
				stateOfDataSource.put("user_name", dataSource.getUsername());
				stateOfDataSource.put("maximum_pool_size", dataSource.getMaximumPoolSize());
				stateOfDataSource.put("minimum_idle", dataSource.getMinimumIdle());
				stateOfDataSource.put("data_source_properties", dataSource.getDataSourceProperties());
				MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
				ObjectName poolName = new ObjectName("com.zaxxer.hikari:type=Pool (" + getPoolName() + ")");
				HikariPoolMXBean poolProxy = JMX.newMXBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
				stateOfDataSource.put("total_connections", poolProxy.getTotalConnections());
				stateOfDataSource.put("idle_connections", poolProxy.getIdleConnections());
				stateOfDataSource.put("active_connections", poolProxy.getActiveConnections());
				stateOfDataSource.put("threads_awaiting_connection", poolProxy.getThreadsAwaitingConnection());
			}
		} catch (Exception e) {
			logger.error("*{}*getStateOfDataSource>>", getPoolName(), e);
		}
		return stateOfDataSource;
	}

	public String getPoolName() {
		return poolName;
	}

	public Map<String, String> getDataSourceProperties() {
		return dataSourceProperties;
	}

	private HikariConfig getJdbcPoolProperties() {
		if (dataSourceProperties == null) {
			return null;
		}
		Properties p = new Properties();
		p.putAll(dataSourceProperties);
		return new HikariConfig(p);
	}

	public DataSource getDataSource() {

		return dataSource;
	}

}
