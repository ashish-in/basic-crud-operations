package org.ncompass.comp.cmutls.helper;

import java.util.HashMap;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class MapBuilder<K, V> {

	private HashMap<K, V> map;

	public MapBuilder() {
		super();
		this.map = new HashMap<K, V>();

	}

	/*
	 * public static <K, V> MapBuilder<K, V> build(K key, V value) {
	 * 
	 * MapBuilder<K, V> mo = new MapBuilder<K, V>(); return mo.put(key, value);
	 * 
	 * }
	 */

	public MapBuilder<K, V> put(K key, V value) {

		map.put(key, value);
		return this;
	}

	public HashMap<K, V> getMap() {
		return map;
	}

}
