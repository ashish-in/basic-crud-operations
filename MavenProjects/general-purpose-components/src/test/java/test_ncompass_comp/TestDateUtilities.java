package test_ncompass_comp;

import java.time.LocalDate;

import org.ncompass.comp.cmutls.staticfns.DateUtilities;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class TestDateUtilities {

	public static void main(String[] args) {

		LocalDate ld = DateUtilities.stringToLocalDate("10/06/1978", "dd/MM/yyyy");

		System.out.println(ld);

	}

}
