package org.ncompass.firstpro.repo.dao.entities.student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.ncompass.comp.cmutls.staticfns.GenericUtilities;
import org.ncompass.firstpro.repo.dao.jdbc.JdbcPoolFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class StudentDao {

	private JdbcTemplate JdbcTemplate;

	/**
	 * @param jdbcTemplate
	 */
	public StudentDao() {
		super();
		JdbcTemplate = JdbcPoolFactory.getInstance().getJdbcTemplate();
	}

	public String create(StudentDto studentDto) {

		String sql = "INSERT INTO STUDENT (NAME, AGE) VALUES (?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();

		JdbcTemplate.update(connection -> {
			PreparedStatement ps = connection.prepareStatement(sql, new String[] { "ID" });
			ps.setString(1, studentDto.getName());
			ps.setInt(2, studentDto.getAge());
			return ps;
		}, keyHolder);

		return keyHolder.getKey().toString();
	}

	public void update(StudentDto studentDto, String id) {

		String sql = "UPDATE STUDENT SET NAME = ?,AGE = ? WHERE ID = ?";
		JdbcTemplate.update(sql, studentDto.getName(), studentDto.getAge(), GenericUtilities.stringToLong(id));

	}

	public void delete(String id) {

		String sql = "DELETE FROM STUDENT WHERE ID = ?";
		JdbcTemplate.update(sql, GenericUtilities.stringToLong(id));

	}

	public StudentDto selectRow(String id) {

		String sql = "SELECT * FROM STUDENT WHERE ID = ?";

		StudentMapper sm = new StudentMapper();
		JdbcTemplate.query(sql, sm, GenericUtilities.stringToLong(id));
		List<StudentDto> list = sm.getList();
		if (GenericUtilities.isEmpty(list)) {
			return null;
		}

		return list.get(0);
	}

	public List<StudentDto> selectList(int offset, int limit) {
		StudentMapper sm = new StudentMapper();
		String sql = "SELECT * FROM STUDENT LIMIT ?, ?";
		JdbcTemplate.query(sql, sm, offset, limit);
		return sm.getList();
	}

}

class StudentMapper implements RowCallbackHandler {

	private List<StudentDto> list;

	/**
	 * @param list
	 */
	public StudentMapper() {
		super();
		this.list = new ArrayList<StudentDto>();
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {

		StudentDto student = new StudentDto();
		student.setId(rs.getString("ID"));
		student.setName(rs.getString("NAME"));
		student.setAge(rs.getInt("age"));

		list.add(student);
	}

	/**
	 * @return the list
	 */
	public List<StudentDto> getList() {
		return list;
	}

}
