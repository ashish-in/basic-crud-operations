package org.ncompass.firstpro.repo.dao.entities.user;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.ncompass.comp.cmutls.staticfns.GenericUtilities;
import org.ncompass.firstpro.repo.dao.jdbc.JdbcPoolFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;


public class UserDao {

	private JdbcTemplate JdbcTemplate;

	/**
	 * @param jdbcTemplate
	 */
	public UserDao() {
		super();
		JdbcTemplate = JdbcPoolFactory.getInstance().getJdbcTemplate();
	}

	public String create(UserDto userDto) {

		String sql = "INSERT INTO USER (DISPLAY_NAME,PHONE_NUMBER,EMAIL,CREATION_DATE,LAST_MOD_DATE) VALUES (?, ?, ?, ?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();

		JdbcTemplate.update(connection -> {
			PreparedStatement ps = connection.prepareStatement(sql, new String[] { "ID" });
			ps.setString(1, userDto.getName());
			ps.setString(2, userDto.getPhoneNumber());
			ps.setString(3, userDto.getEmail());
			ps.setTimestamp(4, userDto.getCreationDate());
			ps.setTimestamp(5, userDto.getLastModDate());
			return ps;
		}, keyHolder);

		return keyHolder.getKey().toString();
	}

	public void update(UserDto userDto, String id) {

		String sql = "UPDATE USER SET DISPLAY_NAME = ?,PHONE_NUMBER = ? WHERE ID = ?";
		JdbcTemplate.update(sql, userDto.getName(), userDto.getPhoneNumber(), GenericUtilities.stringToLong(id));

	}

	public void delete(String id) {

		String sql = "DELETE FROM USER WHERE ID = ?";
		JdbcTemplate.update(sql, GenericUtilities.stringToLong(id));

	}

	public UserDto selectRow(String id) {

		String sql = "SELECT * FROM USER WHERE ID = ?";

		UserMapper sm = new UserMapper();
		JdbcTemplate.query(sql, sm, GenericUtilities.stringToLong(id));
		List<UserDto> list = sm.getList();
		if (GenericUtilities.isEmpty(list)) {
			return null;
		}

		return list.get(0);
	}

	public List<UserDto> selectList(int offset, int limit) {
		UserMapper sm = new UserMapper();
		String sql = "SELECT * FROM USER LIMIT ?, ?";
		JdbcTemplate.query(sql, sm, offset, limit);
		return sm.getList();
	}

}

class UserMapper implements RowCallbackHandler {

	private List<UserDto> list;

	/**
	 * @param list
	 */
	public UserMapper() {
		super();
		this.list = new ArrayList<UserDto>();
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {

		UserDto user = new UserDto();
		user.setId(rs.getString("ID"));
		user.setName(rs.getString("DISPLAY_NAME"));
		user.setPhoneNumber(rs.getString("PHONE_NUMBER"));
		user.setEmail(rs.getString("EMAIL"));
		user.setCreationDate(rs.getTimestamp("CREATION_DATE"));
		user.setLastModDate(rs.getTimestamp("LAST_MOD_DATE"));

		list.add(user);
	}

	/**
	 * @return the list
	 */
	public List<UserDto> getList() {
		return list;
	}

}
