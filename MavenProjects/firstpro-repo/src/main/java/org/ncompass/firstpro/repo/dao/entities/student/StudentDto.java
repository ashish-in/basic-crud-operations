package org.ncompass.firstpro.repo.dao.entities.student;

import org.ncompass.comp.cmutls.helper.PrintLog;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class StudentDto {

	private String id;
	private String name;
	private int age;

	@Override
	public String toString() {

		return PrintLog.buildN("id", id).appendN("name", name).appendN("age", age).toString();

	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

}
