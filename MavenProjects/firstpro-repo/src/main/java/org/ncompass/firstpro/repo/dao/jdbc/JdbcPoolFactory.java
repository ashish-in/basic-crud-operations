package org.ncompass.firstpro.repo.dao.jdbc;

import java.sql.SQLException;
import java.util.Map;

import org.ncompass.comp.cmutls.staticfns.ResourceUtilities;
import org.ncompass.comp.sprgjdbcwrpr.pool.HikariCPPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class JdbcPoolFactory {

	private static final Logger logger = LoggerFactory.getLogger(JdbcPoolFactory.class);
	private static JdbcPoolFactory instance = new JdbcPoolFactory();
	private HikariCPPool jdbcPool;
	private JdbcTemplate jdbcTemplate;

	private JdbcPoolFactory() {

		jdbcPool = new HikariCPPool("jdbcPool", jdbcProperties());

		logger.info("created-instance-JdbcPoolFactory");

	}

	public void createDataSource() throws SQLException {

		jdbcPool.createDataSource();
		testConnection();
		jdbcTemplate = new JdbcTemplate(jdbcPool.getDataSource());

	}

	public void closeDataSource() {

		jdbcPool.closeDataSource();

	}

	public boolean testConnection() throws SQLException {

		return jdbcPool.testConnection();

	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @return the instance
	 */
	public static JdbcPoolFactory getInstance() {
		return instance;
	}

	private Map<String, String> jdbcProperties() {

		Map<String, String> dataSourceProperties = ResourceUtilities.getResourceProperties("jdbc");

		dataSourceProperties.remove("is.password.encrypted");
		return dataSourceProperties;
	}

}
