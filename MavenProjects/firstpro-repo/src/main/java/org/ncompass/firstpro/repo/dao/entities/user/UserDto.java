package org.ncompass.firstpro.repo.dao.entities.user;

import org.ncompass.comp.cmutls.helper.PrintLog;
import java.sql.Timestamp;  
//import java.time.Instant; 


public class UserDto {

	private String ID;
	private String DISPLAY_NAME;
	private String PHONE_NUMBER;
	private String EMAIL;
	private Timestamp CREATION_DATE;
	private Timestamp LAST_MOD_DATE;
	

	@Override
	public String toString() {

		return PrintLog.buildN("id", ID).appendN("name", DISPLAY_NAME).appendN("Phone Number", PHONE_NUMBER).appendN("Email", EMAIL).appendN("Created on", CREATION_DATE).appendN("Modified on", LAST_MOD_DATE).toString();

	}

	/**
	 * @return the id
	 */
	public String getId() {
		return ID;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.ID = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return DISPLAY_NAME;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.DISPLAY_NAME = name;
	}

	/**
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return PHONE_NUMBER;
	}

	/**
	 * @param phone number the phone number to set
	 */
	public void setPhoneNumber(String phNo) {
		this.PHONE_NUMBER = phNo;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return EMAIL;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.EMAIL = email;
	}
	
	/**
	 * @return the CREATION_DATE
	 */
	public Timestamp getCreationDate() {
		return CREATION_DATE;
	}

	/**
	 * @param CREATION_DATE the CREATION_DATE to set
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.CREATION_DATE = creationDate;
	}

	/**
	 * @return the LAST_MOD_DATE
	 * 
	 */
	public Timestamp getLastModDate() {
		return LAST_MOD_DATE;
	}

	/**
	 * @param LAST_MOD_DATE the LAST_MOD_DATE to set
	 */
	public void setLastModDate(Timestamp modifyDate) {
		this.LAST_MOD_DATE = modifyDate;
	}

}
