package org.ncompass.firstpro.repo.utilities;

import org.ncompass.firstpro.repo.dao.jdbc.JdbcPoolFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class RepositoryUpAndDown {

	private static final Logger logger = LoggerFactory.getLogger(RepositoryUpAndDown.class);

	public static void repositoryUp() throws Exception {

		JdbcPoolFactory.getInstance().createDataSource();
		logger.info(">>repositoryUp>>testConnection>>" + JdbcPoolFactory.getInstance().testConnection());

		logger.info("============repositoryUpgood============");

	}

	public static void repositoryDown() {

		JdbcPoolFactory.getInstance().closeDataSource();

		logger.info("============repositoryDowngood============");

	}

}
