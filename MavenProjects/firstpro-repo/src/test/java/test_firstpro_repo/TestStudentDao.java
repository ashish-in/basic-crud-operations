package test_firstpro_repo;

import java.util.List;
import org.ncompass.comp.sprgjdbcwrpr.utilities.JdbcUtilities;
import org.ncompass.firstpro.repo.dao.entities.student.StudentDao;
import org.ncompass.firstpro.repo.dao.entities.student.StudentDto;
import org.ncompass.firstpro.repo.dao.jdbc.JdbcPoolFactory;
import org.ncompass.firstpro.repo.utilities.RepositoryUpAndDown;
import org.springframework.dao.DataAccessException;

/**
 * @author anoop
 * @company NCompass
 * @date Mar 15, 2022
 **/
public class TestStudentDao {

	
	public static void main(String[] args) throws Exception {

		try {
			RepositoryUpAndDown.repositoryUp();

			System.out.println("testConnection>>" + JdbcPoolFactory.getInstance().testConnection());

			create();

			// update();

			// delete();

			// selectRow();

			// selectList();

		} catch (DataAccessException dae) {

			System.err.println(
					"catch throned exception DataAccessException>>" + JdbcUtilities.getDataAccessErrorMessage(dae));
		}

		finally {
			RepositoryUpAndDown.repositoryDown();

		}

	}

	public static void create() {

		StudentDto studentDto = new StudentDto();
		studentDto.setName("Noah_" + System.currentTimeMillis());
		studentDto.setAge(12);

		StudentDao studentDao = new StudentDao();
		String id = studentDao.create(studentDto);
		System.out.println("id>>" + id);

	}

	public static void update() {

		StudentDto studentDto = new StudentDto();
		studentDto.setName("Noah_" + System.currentTimeMillis());
		studentDto.setAge(12);

		StudentDao studentDao = new StudentDao();
		studentDao.update(studentDto, "4");

	}

	public static void delete() {

		StudentDao studentDao = new StudentDao();
		studentDao.delete("4");

	}

	public static void selectRow() {

		StudentDao studentDao = new StudentDao();
		StudentDto row = studentDao.selectRow("5");
		System.out.println("row>>" + row);

	}

	public static void selectList() {

		StudentDao studentDao = new StudentDao();
		List<StudentDto> list = studentDao.selectList(0, 10);
		System.out.println("list>>" + list);

	}

}
