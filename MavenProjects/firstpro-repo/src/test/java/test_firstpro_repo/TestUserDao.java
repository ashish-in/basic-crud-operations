package test_firstpro_repo;

import java.util.List;
import java.sql.Timestamp;  
import java.time.Instant; 
import org.ncompass.comp.sprgjdbcwrpr.utilities.JdbcUtilities;
import org.ncompass.firstpro.repo.dao.entities.user.UserDao;
import org.ncompass.firstpro.repo.dao.entities.user.UserDto;
import org.ncompass.firstpro.repo.dao.jdbc.JdbcPoolFactory;
import org.ncompass.firstpro.repo.utilities.RepositoryUpAndDown;
import org.springframework.dao.DataAccessException;

public class TestUserDao {

	
	public static void main(String[] args) throws Exception {

		try {
			RepositoryUpAndDown.repositoryUp();

			System.out.println("testConnection>>" + JdbcPoolFactory.getInstance().testConnection());

//			create();

//			 update();

//			 delete();

//			 selectRow();

			 selectList();

		} catch (DataAccessException dae) {

			System.err.println(
					"catch throned exception DataAccessException>>" + JdbcUtilities.getDataAccessErrorMessage(dae));
		}

		finally {
			RepositoryUpAndDown.repositoryDown();

		}

	}

	public static void create() {

		UserDto userDto = new UserDto();
		userDto.setName("Noah_" + System.currentTimeMillis());
		userDto.setPhoneNumber("9956845465");
		userDto.setEmail("ABCD@GMAIL.COM");
		Timestamp instant1= Timestamp.from(Instant.now()); 
		userDto.setCreationDate(instant1);
		Timestamp instant2= Timestamp.from(Instant.now()); 
		userDto.setLastModDate(instant2);

		UserDao userDao = new UserDao();
		String id = userDao.create(userDto);
		System.out.println("id>>" + id);

	}

	public static void update() {

		UserDto userDto = new UserDto();
		userDto.setName("Noah_" + System.currentTimeMillis());
		userDto.setPhoneNumber("9955555465");

		UserDao userDao = new UserDao();
		userDao.update(userDto, "1");

	}

	public static void delete() {

		UserDao userDao = new UserDao();
		userDao.delete("1");

	}

	public static void selectRow() {

		UserDao userDao = new UserDao();
		UserDto row = userDao.selectRow("1");
		System.out.println("row>>" + row);

	}

	public static void selectList() {

		UserDao userDao = new UserDao();
		List<UserDto> list = userDao.selectList(0, 10);
		System.out.println("list>>" + list);

	}

}
